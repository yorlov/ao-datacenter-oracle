package com.stiltsoft.bitbucket.cop1;

public class TaskStatistics {

    private long repositories;
    private int indexCommitsTasks;
    private int indexFileSizesTasks;

    public TaskStatistics(long repositories, int indexCommitsTasks, int indexFileSizesTasks) {
        this.repositories = repositories;
        this.indexCommitsTasks = indexCommitsTasks;
        this.indexFileSizesTasks = indexFileSizesTasks;
    }

    public long getRepositories() {
        return repositories;
    }

    public int getIndexCommitsTasks() {
        return indexCommitsTasks;
    }

    public int getIndexFileSizesTasks() {
        return indexFileSizesTasks;
    }
}