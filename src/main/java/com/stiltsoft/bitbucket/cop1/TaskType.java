package com.stiltsoft.bitbucket.cop1;

public enum TaskType {

    INDEX_COMMITS(0), INDEX_FILE_SIZES(1);

    private int id;

    TaskType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

}