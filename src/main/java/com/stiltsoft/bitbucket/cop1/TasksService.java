package com.stiltsoft.bitbucket.cop1;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.atlassian.bitbucket.util.PageUtils.toStream;
import static com.stiltsoft.bitbucket.cop1.TaskType.INDEX_COMMITS;
import static com.stiltsoft.bitbucket.cop1.TaskType.INDEX_FILE_SIZES;

@Component
public class TasksService {

    private RepositoryService repositoryService;
    private TasksDao dao;
    private TransactionTemplate transactionTemplate;

    @Autowired
    public TasksService(RepositoryService repositoryService,
                        TasksDao dao,
                        @ComponentImport("salTransactionTemplate") TransactionTemplate transactionTemplate) {
        this.repositoryService = repositoryService;
        this.dao = dao;
        this.transactionTemplate = transactionTemplate;
    }

    public void indexCommits(Repository repository) {
        schedule(repository, INDEX_COMMITS);
    }

    public void indexFileSizes(Repository repository) {
        schedule(repository, INDEX_FILE_SIZES);
    }

    public TaskStatistics fetchStatistics() {
        return new TaskStatistics(
                toStream(repositoryService::findAll, 100).count(),
                dao.tasksCount(INDEX_COMMITS.getId()),
                dao.tasksCount(INDEX_FILE_SIZES.getId())
        );
    }

    private void schedule(Repository repository, TaskType taskType) {
        int repo = repository.getId();
        int type = taskType.getId();

        dao.fetchTask(repo, type).orElseGet(() -> {
            try {
                return transactionTemplate.execute(() -> dao.createTask(repo, type));
            } catch (Exception e) {
                if (isUniqueConstraint(e)) {
                    return dao.fetchTask(repo, type).get();
                }
                throw new RuntimeException(e);
            }
        });
    }

    private static boolean isUniqueConstraint(Exception exception) {
        return exception.getMessage().toLowerCase().contains("u_ao_bc6212_tasks_unique_id");
    }
}