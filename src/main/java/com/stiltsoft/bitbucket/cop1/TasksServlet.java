package com.stiltsoft.bitbucket.cop1;

import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.bitbucket.user.UserService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.ImmutableMap;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;

public class TasksServlet extends HttpServlet {

    private SoyTemplateRenderer soyTemplateRenderer;
    private UserService userService;
    private TasksService tasksService;

    public TasksServlet(@ComponentImport SoyTemplateRenderer soyTemplateRenderer, UserService userService, TasksService tasksService) {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.userService = userService;
        this.tasksService = tasksService;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String slug = request.getPathInfo().substring(1);

        ApplicationUser user = userService.getUserBySlug(slug);

        if (user == null) {
            response.sendError(SC_NOT_FOUND);
            return;
        }

        response.setContentType("text/html;charset=UTF-8");

        TaskStatistics statistics = tasksService.fetchStatistics();

        soyTemplateRenderer.render(
                response.getWriter(),
                "com.stiltsoft.bitbucket.cop1:cop1-templates",
                "cop1.page",
                ImmutableMap.of(
                        "user", user,
                        "statistics", ImmutableMap.of(
                                "repositories", statistics.getRepositories(),
                                "indexCommits", statistics.getIndexCommitsTasks(),
                                "indexFileSizes", statistics.getIndexFileSizesTasks()
                        )
                )
        );
    }
}