package com.stiltsoft.bitbucket.cop1;

import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.Table;
import net.java.ao.schema.Unique;

import static com.stiltsoft.bitbucket.cop1.AoTask.*;

@Table(TABLE_NAME)
public interface AoTask extends Entity {

    String TABLE_NAME = "TASKS";
    String REPOSITORY_COLUMN = "REPOSITORY";
    String CREATED_AT_COLUMN = "CREATED_AT";
    String TYPE_COLUMN = "TYPE";
    String UNIQUE_ID_COLUMN = "UNIQUE_ID";

    String[] ALL = new String[]{REPOSITORY_COLUMN, CREATED_AT_COLUMN, TYPE_COLUMN, UNIQUE_ID_COLUMN};

    @Accessor(REPOSITORY_COLUMN)
    @Indexed
    int getRepository();

    @Accessor(CREATED_AT_COLUMN)
    long getCreatedAt();

    @Accessor(TYPE_COLUMN)
    int getType();

    @Deprecated
    @Unique
    @Accessor(UNIQUE_ID_COLUMN)
    String getUniqueId();
}