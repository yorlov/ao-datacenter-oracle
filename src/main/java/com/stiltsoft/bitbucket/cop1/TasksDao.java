package com.stiltsoft.bitbucket.cop1;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.ImmutableMap;
import net.java.ao.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.atlassian.bitbucket.ao.AoUtils.newQuery;
import static com.stiltsoft.bitbucket.cop1.AoTask.*;
import static java.lang.System.currentTimeMillis;
import static java.util.Arrays.stream;

@Component
public class TasksDao {

    private ActiveObjects ao;

    @Autowired
    public TasksDao(@ComponentImport ActiveObjects ao) {
        this.ao = ao;
    }

    public Optional<AoTask> fetchTask(int repository, int type) {
        Query query = newQuery(AoTask.ALL)
                .where(REPOSITORY_COLUMN + " = ? AND " + TYPE_COLUMN + " = ?", repository, type)
                .limit(1);
        return stream(ao.find(AoTask.class, query)).findFirst();
    }

    public int tasksCount(int type) {
        return ao.count(AoTask.class, TYPE_COLUMN + " = ?", type);
    }

    public AoTask createTask(int repository, int type) {
        return ao.create(AoTask.class, ImmutableMap.of(
                REPOSITORY_COLUMN, repository,
                CREATED_AT_COLUMN, currentTimeMillis(),
                TYPE_COLUMN, type,
                UNIQUE_ID_COLUMN, repository + "_" + type)
        );
    }
}